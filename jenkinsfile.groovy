pipeline {
    agent any

    stages {
        stage("Build") {
            steps {
                echo "Start compiling..."
                script {
                    sh "mvn clean install "
                }
            }
        }
        stage("Unit Test") {
            steps {
                echo "Start unit test"
                script {
                    sh "mvn clean test"
                }
            }
            post {
                always {
                    allure results: [[path: 'allure-results']]
                }
            }
        }


    }

}
