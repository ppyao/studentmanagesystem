package com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2;


import com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.entity.Student;
import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import net.minidev.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;


import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringBoot2JpaWithHibernateAndH2Application.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StudentApiTest {

    @LocalServerPort
    private int port;

    private final ThreadLocal<Map<String, File>> cache = ThreadLocal.withInitial(ConcurrentHashMap::new);

    @Before
    public void before() {
        RestAssured.port = port;
        RestAssured.requestSpecification = new RequestSpecBuilder()
                .setContentType("application/json")
                .build();
    }


    @Test
    @Description("Should Get All Student Information")
    public void shouldGetAllStudentInformation() {
        final Response response = RestAssured.get("/api/students");
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }

    @Test
    @Description("Should Get Specific Student Information based on student name")
    public void shouldGetSpecificStudentInformationBasedOnName() {
        createBookAsUri();
        given()
                .when()
                .get("api/students/name/{name}", "Pingping Yao")
                .then()
                .statusCode(200);
    }


    private String createBookAsUri() {

        JSONObject requestParams = new JSONObject();
        requestParams.put("name", "Pingping Yao");
        requestParams.put("passportNumber", "085234");


        return given()
                .contentType(ContentType.JSON)
                .body(requestParams.toJSONString())
                .when()
                .post("/api/students")
                .then()
                .statusCode(201)
                .extract().path("id").toString();

    }
}
