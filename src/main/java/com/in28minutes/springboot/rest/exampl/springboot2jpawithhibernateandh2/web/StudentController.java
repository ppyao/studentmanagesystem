package com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.web;

import com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.entity.Student;
import com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.repo.StudentRepository;
import com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.web.exception.StudentMismatchException;
import com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.web.exception.StudentNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/students")
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @GetMapping
    public Iterable<Student> findAll() {
        return studentRepository.findAll();
    }

    @GetMapping("/name/{name}")
    public List<Student> findByName(@PathVariable String name) {
        return studentRepository.findByName(name);
    }

    @GetMapping("/{id}")
    public Student findOne(@PathVariable long id) {
        return studentRepository.findById(id)
                .orElseThrow(StudentNotFoundException::new);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Student create(@RequestBody Student student) {
        Student student1 = studentRepository.save(student);
        return student1;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        studentRepository.findById(id)
                .orElseThrow(StudentNotFoundException::new);
        studentRepository.deleteById(id);
    }

    @PutMapping("/{id}")
    public Student updateStudent(@RequestBody Student student, @PathVariable long id) {
        if (student.getId() != id) {
            throw new StudentMismatchException();
        }

        studentRepository.findById(id)
                .orElseThrow(StudentNotFoundException::new);

        return studentRepository.save(student);
    }



}