package com.in28minutes.springboot.rest.exampl.springboot2jpawithhibernateandh2.web.exception;

public class StudentMismatchException extends RuntimeException {

    public StudentMismatchException() {
        super();
    }

    public StudentMismatchException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public StudentMismatchException(final String message) {
        super(message);
    }

    public StudentMismatchException(final Throwable cause) {
        super(cause);
    }
}